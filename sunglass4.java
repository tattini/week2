package edu.phoenix.mbl402.week2apptt2163;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class sunglass4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunglass5);
        Button button = (Button) findViewById(R.id.viewcart);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openCart();
            }
        });
    }
    public void openCart() {

        Intent intent = new Intent(this, cart.class);
        startActivity(intent);
    }
}
