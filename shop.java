package edu.phoenix.mbl402.week2apptt2163;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class shop extends AppCompatActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        button = (Button) findViewById(R.id.sunglass1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSunglass1();
            }
        });

        button = (Button) findViewById(R.id.sunglass2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSunglass2();
            }
        });

        button = (Button) findViewById(R.id.sunglass3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSunglass3();
            }
        });

        button = (Button) findViewById(R.id.sunglass4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSunglass4();
            }
        });

        button = (Button) findViewById(R.id.sunglass5);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSunglass5();
            }
        });
    }
    public void openSunglass1() {
        Intent intent = new Intent(this, sunglass1.class);
        startActivity(intent);
    }

    public void openSunglass2() {
        Intent intent = new Intent(this, sunglass2.class);
        startActivity(intent);
    }

    public void openSunglass3() {
        Intent intent = new Intent(this, sunglass3.class);
        startActivity(intent);
    }
    public void openSunglass4() {
        Intent intent = new Intent(this, sunglass4.class);
        startActivity(intent);
    }
    public void openSunglass5() {
        Intent intent = new Intent(this, sunglass5.class);
        startActivity(intent);
    }
}
