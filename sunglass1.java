package edu.phoenix.mbl402.week2apptt2163;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class sunglass1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sunglass2);

        Button button = (Button) findViewById(R.id.addcart);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
            }
        });

        button = (Button) findViewById(R.id.viewcart);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openCart();
            }
        });
    }
    public void openCart() {

        Intent intent = new Intent(this, cart.class);
        startActivity(intent);
    }
}
